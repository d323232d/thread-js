class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async setReaction(userId, query) {
    // define the callback for future use as a promise
    // const updateOrDelete = react => (react.isLike === isLike
    //   ? this._postReactionRepository.deleteById(react.id)
    //   : this._postReactionRepository.updateById(react.id, { isLike }));
    const { postId, isLike, isDislike } = query;

    const upOrDel = react => ((react.isDislike === isDislike || react.isLike === isLike)
      // && (react.isLike === isDislike || react.isDislike === isLike)
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike, isDislike })
    );

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await upOrDel(reaction) // updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike, isDislike }); // , isLike

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
  }
}

export { Post };
