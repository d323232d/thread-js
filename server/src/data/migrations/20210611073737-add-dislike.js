
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('postReactions', 'isDislike', {
        type: Sequelize.BOOLEAN
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('postReactions', 'isDislike', { transaction })
    ]))
};
